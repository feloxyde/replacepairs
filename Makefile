

test : clean 
	./replacepairs.py testinput.txt test.pairs testoutput.txt
	diff testoutput.txt testoutput.exp
	- rm testoutput.txt

clean :
	- rm testoutput.txt | true

.PHONY: clean test
