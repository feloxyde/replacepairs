# replacepairs.py

A simple and straightforward really simple Python script that replaces strings in a file according to rules written in a config file. Actually, this tool may be *too simple*,
as it is only able to replace plain strings, and do not work with regex. It has not been extensively tested, so please beware.

# How to install 

- install Python3 
- download `replacepairs.py` file and place it somewhere convenient for you to use, for exemple at the root of your project 

# How to invoke 

```
python3 replacepairs.py infile rulefile outfile 
```

- `infile` is an existing file you want to replace some content from 
- `rulefile` is a file with pairs target/replace written in according to syntax described below
- `outfile` is the name of a non-existing file that will be used to output `infile` content with replaced stuff. 

# Rules Syntax

## General syntax

Rules are written in plain text in a file, with whatever extension. In the test exemple I used `.pairs`, but it doesn't matter.

The syntax for a file is fairly simple : one rule per line

```
rule1
rule2
rule3
...
```

With a rule being expressed like :

```
sTARGETsREPLACEs
```
where  : 
- `s` is a character used for separation, can be different for each rule
- `TARGET` is a sequence of characters not containing `s`, that will be looked for
- `REPLACE` is a sequence of characters not containing `s`, that will used to replace `TARGET` in file. 

Example of a valid set of rules :

```
!replaceme!byme!
 replaceit bythis 
AswapthisAwiththatA
:andthat:withit:
```

Please note that when using space at separator, you need to put a space before `TARGET` and also after `REPLACE`

## Comments

Anything following the third separator on the line of a rule will be ignored. 

```
!replaceme!byme!here the string is completely ignored
```

It can be used as comments in case you want to remember what rule was for.

```
!#FFOOFF!#25OAF3! replace color for titles
```

## Rule execution order

Rules are executed from down to bottom, on full file content each time. Let's see the following text. 

```
AAA BBB CCC DDD AAA EEE
```

Given the following set of rule : 

```
!AAA!BBB!      (rule 1)
!BBB EEE!CCC!  (rule 2)
!CCC!DDD!      (rule 3)
```

The sequence of transformation will be : 

- initial state     : `AAA BBB CCC DDD AAA EEE`
- after rule 1 pass : `BBB BBB CCC DDD BBB EEE`
- after rule 2 pass : `BBB BBB CCC DDD CCC`
- after rule 3 pass : `AAA BBB DDD DDD DDD`
- final state       : `AAA BBB DDD DDD DDD`

Here we can see that character sequences created by rule 1 can still be targeted by rule 2 and 3, because they follow in order. 

# EWQ (Eventually Wondered Questions)


### Why did you code that ? 

Actually, I was looking for a way to define color variables in Draw.io, to ease design of some complex diagram, to be able to easily change style, as I would do with CSS variables. 
Unfortunately, it seems that this feature will never be in there, so I thought about using color codes as an actual replacement for variable names.

- First use some fairly distinct values for colors, as #FF00FF, defined as sort of label
- Export diagram as XML
- Replace color values in XML, as for example #FF00FF -> #02F0AD 
- Gen SVG or other using XML with right values 

replacepairs was coded especially with that in mind : associating pairs of quite abstract values in a specific file, allowing to easily select a color scheme to apply to a diagram. 


### Is the tool fast ?

Given it does one pass per rule, with several array and string operations, I would definitely not bet on it in a contest. For most use it should be sufficient tho.
