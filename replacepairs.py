#!/bin/python3

from sys import exit
from sys import argv
from os import path


class Pair:
    def __init__(self, target, replace):
        self.tar = target
        self.rep = replace

    def __str__(self):
        return self.tar + " -> " + self.rep

    def __repr__(self):
        return self.tar + " -> " + self.rep


def load_pairs(filename):
    pairs = []
    with open(filename, "r") as file:
        lines = file.readlines()
    for line in lines:
        sep = line[0]
        words = line.split(sep)
        if len(words) < 4:
            print("rules shall be sTARGETsREPLACEs (s = separator), found ", line)
            print("decomposed into array : ", words)
            exit(1)
        pairs.append(Pair(words[1], words[2]))
    return pairs


def replace_in(string, pairs):
    for pair in pairs:
        spl = string.split(pair.tar)
        string = pair.rep.join(spl)
    return string


if len(argv) != 4:
    print("command shall be invoked with three arguments : infile rulefile outfile, found ", argv, " instead")
    exit(1)

infile = argv[1]
rulefile = argv[2]
outfile = argv[3]

if path.exists(outfile):
    print("outfile ", outfile, " already exists, aborting")
    exit(1)
if not path.exists(infile) or not path.isfile(infile):
    print("infile ", infile, " does not exist, or is a directory, aborting")

pairs = load_pairs(rulefile)

with open(infile, "r") as inf:
    instr = inf.read()

outstr = replace_in(instr, pairs)

with open(outfile, "w") as ouf:
    ouf.write(outstr)
